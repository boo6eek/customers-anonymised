# Сustomers-anonymised

## Installation

Git clone

```
git clone https://gitlab.com/boo6eek/customers-anonymised.git
```

Create .env file with DB_URI

```
echo "DB_URI=mongodb://127.0.0.1:27017/customers-anonymised" | tee .env
```

Install dependencies

```
yarn install
```

## Before Usage
This application use [Change Streams MongoDB](https://www.mongodb.com/docs/manual/changeStreams/)

Example `mongod.conf`
```
systemLog:
  destination: file
  path: /opt/homebrew/var/log/mongodb/mongo.log
  logAppend: true
storage:
  dbPath: /opt/homebrew/var/mongodb
net:
  bindIp: 127.0.0.1
replication:
  replSetName: "rs0"
```

## Usage

Realtime generate

```
yarn app
```

Reindex and realtime sync

```
yarn sync 
```

Reindex only 

```
yarn sync --full-reindex
```
