import { Schema, model, Model } from 'mongoose';
import { CustomerModel } from '../interfaces/customer';

const CustomerSchema = new Schema<CustomerModel, Model<CustomerModel>>(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    address: {
      line1: { type: String, required: true },
      line2: { type: String, required: true },
      postcode: { type: String, required: true },
      city: { type: String, required: true },
      state: { type: String, required: true },
      country: { type: String, required: true },
    },
    createdAt: { type: Date, required: true },
  },
  {
    collection: 'customers-nonymised',
    timestamps: false,
  },
);

export default model<CustomerModel>('CustomerAnonymised', CustomerSchema);
