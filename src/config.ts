import 'dotenv/config';

const config: { DB_URI: string; FULL_REINDEX: boolean } = {
  DB_URI: process.env.DB_URI,
  FULL_REINDEX: process.argv.includes('--full-reindex'),
};

export default config;
