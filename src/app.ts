import 'dotenv/config';
import { connect } from 'mongoose';
import config from './config';
import { getRandomInt, mockCustomer } from './utils';
import Customer from './models/Customer';

(async () => {
  if (!config.DB_URI) {
    throw new Error('DB_URI is undefined.');
  }

  await connect(config.DB_URI);

  console.log('Connect to mongodb.');

  setInterval(async () => {
    const count = getRandomInt(1, 10);
    const customers = [];

    for (let i = 0; i < count; i++) {
      customers.push(mockCustomer());
    }

    await Customer.insertMany(customers);

    console.log(`Created ${customers.length} customers`);
  }, 200);
})();
