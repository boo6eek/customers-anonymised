import { Customer, CustomerModel } from './interfaces/customer';
import { faker } from '@faker-js/faker';

export const getRandomInt = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getRandomStr = (length: number): string => {
  const chrs = 'abdehkmnpswxzABDEFGHKMNPQRSTWXZ1234567890';
  let str = '';
  for (let i = 0; i < length; i++) {
    const pos = getRandomInt(1, chrs.length);
    str += chrs.substring(pos, pos + 1);
  }
  return str;
};

export const mockCustomer = (): Customer => ({
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  email: faker.internet.email(),
  address: {
    line1: faker.address.secondaryAddress(),
    line2: faker.address.secondaryAddress(),
    postcode: faker.address.zipCode(),
    city: faker.address.city(),
    state: faker.address.state(),
    country: faker.address.country(),
  },
});

export const makeCustomerAnonymised = (customer: CustomerModel): CustomerModel => ({
  _id: customer._id,
  firstName: getRandomStr(8),
  lastName: getRandomStr(8),
  email: customer.email.replace(/[^@]*/i, getRandomStr(8)),
  address: {
    line1: getRandomStr(8),
    line2: getRandomStr(8),
    postcode: customer.address.postcode,
    city: customer.address.city,
    state: customer.address.state,
    country: customer.address.country,
  },
  createdAt: customer.createdAt,
});
