import 'dotenv/config'
import { connect, UpdateQuery } from 'mongoose'
import { AnyBulkWriteOperation } from 'mongodb'
import config from './config'
import { CustomerModel } from './interfaces/customer'
import Customer from './models/Customer'
import CustomerAnonymised from './models/CustomerAnonymised'
import { makeCustomerAnonymised } from './utils'

const handleWatch = () => {
  let newCustomers: AnyBulkWriteOperation<CustomerModel>[] = []

  const handleSave = async () => {
    const cloneNewCustomers: AnyBulkWriteOperation<CustomerModel>[] = [ ...newCustomers ]

    newCustomers = []

    if (cloneNewCustomers.length) {
      await CustomerAnonymised.bulkWrite(cloneNewCustomers)
    }

    console.log('CustomerAnonymised indexing count => ', cloneNewCustomers.length)
  }

  const CustomerChangeStream = Customer.watch()

  CustomerChangeStream.on('change', async (next: { fullDocument?: CustomerModel }) => {
    if (next?.fullDocument) {
      newCustomers.push({
        updateOne: {
          filter: {_id: next.fullDocument._id},
          update: makeCustomerAnonymised(next.fullDocument),
          upsert: true
        }
      })

      if (newCustomers.length > 1000) {
        await handleSave()
      }
    }
  })

  setInterval(handleSave, 1000)
};

(async () => {
  if (!config.DB_URI) {
    throw new Error('DB_URI is undefined.')
  }

  await connect(config.DB_URI)

  console.log('Connect to mongodb.')

  const reindexCustomers: AnyBulkWriteOperation<CustomerModel>[] = []

  const lastUCustomer: CustomerModel[] = await Customer.find().sort({createdAt: -1}).limit(1).exec()
  const lastUCustomerAnonymised: CustomerModel[] = await CustomerAnonymised.find().sort({createdAt: -1}).limit(1).exec()

  if (!config.FULL_REINDEX) {
    handleWatch()
  }

  const query: UpdateQuery<CustomerModel> = {}

  if (lastUCustomer.length) {
    query.createdAt = {
      $lte: lastUCustomer[0].createdAt
    }

    if (lastUCustomerAnonymised.length) {
      query.createdAt.$gt = lastUCustomerAnonymised[0].createdAt
    }

    await Customer.find(query).cursor().eachAsync((doc: CustomerModel) => {
      reindexCustomers.push({
        updateOne: {
          filter: {_id: doc._id},
          update: makeCustomerAnonymised(doc),
          upsert: true
        }
      })
    })
  }

  const limitDocs = 1000

  if (reindexCustomers.length) {
    const countCycle = Math.ceil(reindexCustomers.length / limitDocs)

    for (let i = 0; i < countCycle; i++) {
      const startIndex = i * limitDocs
      await CustomerAnonymised.bulkWrite(reindexCustomers.slice(startIndex, startIndex + limitDocs))
    }

    console.log('CustomerAnonymised reindex count => ', reindexCustomers.length)
  } else {
    console.log('CustomerAnonymised already reindex')
  }

  if (config.FULL_REINDEX) {
    process.exit(0)
  }
})()
