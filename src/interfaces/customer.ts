export interface CustomerModel extends Customer {
  _id: string;
  createdAt: Date;
}

export interface Customer {
  firstName: string;
  lastName: string;
  email: string;
  address: Address;
}

export interface Address {
  line1: string;
  line2: string;
  postcode: string;
  city: string;
  state: string;
  country: string;
}
